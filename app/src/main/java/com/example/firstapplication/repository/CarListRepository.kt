package com.example.firstapplication.repository

import android.content.Context
import com.example.firstapplication.local_db.CarListDao
import com.example.firstapplication.local_db.MyRoomDataBase
import com.example.firstapplication.model.CarData
import com.example.firstapplication.model.CarModel
import com.example.firstapplication.model.CarProsCons
import com.example.firstapplication.utils.AppUtils
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.*
import java.lang.reflect.Type

class CarListRepository private constructor() : CarRepositoryInterface {

    private lateinit var carListDao: CarListDao

    companion object {
        private var INSTANCE: CarListRepository? = null

        /*@Method to create singleton instance
        * @return instance of CarListrepo
        * */
        fun getRepositoryInstance(): CarListRepository? {
            if (INSTANCE == null) {
                INSTANCE = CarListRepository()
            }
            return INSTANCE
        }
    }

    override suspend fun loadJSON(context: Context): MutableList<CarData> {
        carListDao = MyRoomDataBase.getAppDatabase(context)!!.carListDao()!!

        val listLocalDB: Deferred<MutableList<CarData>> =
            CoroutineScope(Dispatchers.IO).async(Dispatchers.IO) {
                carListDao.getCarList()
            }

        if (!listLocalDB.await().isNullOrEmpty()) {
            return listLocalDB.await()
        }

        return carListDataFromAssets(context)
    }

    /*
    * Method to get json from assets
    * @params  context
    * @return MutableList<CarData>
    * */
    private suspend fun carListDataFromAssets(context: Context): MutableList<CarData> {
        // Getting json data from the car_list json file
        val listAssets: Deferred<MutableList<CarData>> =
            CoroutineScope(Dispatchers.IO).async(Dispatchers.IO) {

                val json = AppUtils.getLoadAssetsJson(context)

                val listType: Type = object :
                    TypeToken<ArrayList<CarModel>>() {}.type // create Type of response as per
                val carModel = AppUtils.getCustomGson()
                    .fromJson<ArrayList<CarModel>>(json, listType) // convert json data to model

                val carsData = ArrayList<CarData>()

                // create data as per requirement for expandable recyclerview
                for (item in carModel.indices) {
                    val carPros = ArrayList<CarProsCons>()
                    carPros.add(CarProsCons(label = "Pros:", car_pros = carModel[item].prosList))
                    carPros.add(CarProsCons(label = "Cons:", car_pros = carModel[item].consList))
                    carsData.add(
                        CarData(
                            carModel[item].model!!,
                            carModel[item].make,
                            AppUtils.getDrawableImage(item),
                            carModel[item].marketPrice,
                            carModel[item].rating,
                            0,
                            false,
                            carPros
                        )
                    )
                }

                carListDao.insertAll(carsData) // Insert data in the local DB

                return@async carsData
            }

        return listAssets.await()
    }
}

interface CarRepositoryInterface{

    /*
    * Method to get json from assets and local db
    * @params  context
    * @return MutableList<CarData>
    * */
    suspend fun loadJSON(context: Context): MutableList<CarData>
}