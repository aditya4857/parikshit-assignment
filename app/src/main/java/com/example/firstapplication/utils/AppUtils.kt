package com.example.firstapplication.utils

import android.content.Context
import com.example.firstapplication.R
import com.example.firstapplication.model.CarData
import com.example.firstapplication.model.CarModel
import com.example.firstapplication.model.CarProsCons
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import java.io.IOException
import java.io.InputStream
import java.lang.reflect.Type
import java.nio.charset.Charset

class AppUtils {
    companion object {

        /*
        * @Method get the of common object of Gson
        * @return Gson
        * */
        fun getCustomGson(): Gson {
            val gb = GsonBuilder()
            return gb.create()
        }

        /*
         * Method to get json from assets
         * @params  context
         * @return string of json
         * */
        fun getLoadAssetsJson(context: Context): String? {

            var json: String? = null
            json = try {
                val `is`: InputStream = context.assets.open("car_list.json")
                val size: Int = `is`.available()
                val buffer = ByteArray(size)
                `is`.read(buffer)
                `is`.close()
                val charset: Charset = Charsets.UTF_8
                String(buffer, charset)
            } catch (ex: IOException) {
                ex.printStackTrace()
                return null
            }
            return json
        }

        /*
         * Method to get json from assets
         * @params  context
         * @return MutableList<CarData>
         * */
        fun getLoadJsonFromAssets(context: Context): MutableList<CarData> {

            var json: String? = null
            try {
                val `is`: InputStream = context.assets.open("car_list.json")
                val size: Int = `is`.available()
                val buffer = ByteArray(size)
                `is`.read(buffer)
                `is`.close()
                val charset: Charset = Charsets.UTF_8
                json = String(buffer, charset)
            } catch (ex: IOException) {
                ex.printStackTrace()
            }

            val listType: Type =
                object : TypeToken<ArrayList<CarModel>>() {}.type // create Type of response as per
            val carModel = getCustomGson().fromJson<ArrayList<CarModel>>(
                json,
                listType
            ) // convert json data to model

            val carsData = ArrayList<CarData>()

            // create data as per requirement for expandable recyclerview
            for (item in carModel.indices) {
                val carPros = ArrayList<CarProsCons>()
                carPros.add(CarProsCons(label = "Pros:", car_pros = carModel[item].prosList))
                carPros.add(CarProsCons(label = "Cons:", car_pros = carModel[item].consList))
                carsData.add(
                    CarData(
                        carModel[item].model!!,
                        carModel[item].make,
                        getDrawableImage(item),
                        carModel[item].marketPrice,
                        carModel[item].rating,
                        0,
                        false,
                        carPros
                    )
                )
            }

            return carsData
        }

        /*
         * @Method for getting image from resource file
         * @params Int position of car list
         * @return drawable image
         * */
        fun getDrawableImage(position: Int): Int {
            val image = when (position) {
                0 -> R.drawable.range_rover
                1 -> R.drawable.alpine_roadster
                2 -> R.drawable.bmw
                3 -> R.drawable.mercedez_benz_glc
                else -> R.drawable.tacoma
            }
            return image
        }
    }
}