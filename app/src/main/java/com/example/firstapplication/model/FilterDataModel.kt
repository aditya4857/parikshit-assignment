package com.example.firstapplication.model

data class FilterDataModel(var car_make: String? = null, var car_model: String? = null)
