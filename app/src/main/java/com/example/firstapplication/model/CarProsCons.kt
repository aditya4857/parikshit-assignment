package com.example.firstapplication.model

data class CarProsCons(
    val label: String,
    val car_pros: ArrayList<String>? = null
)