package com.example.firstapplication.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.firstapplication.R
import com.example.firstapplication.databinding.CollapseItemLayoutBinding
import com.example.firstapplication.databinding.ExpandableItemLayoutBinding
import com.example.firstapplication.model.CarData
import com.example.firstapplication.model.CarProsCons

class RecyclerViewAdapter(private var mContext: Context, private var list: MutableList<CarData>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val viewPool = RecyclerView.RecycledViewPool()

    class GroupViewHolder(row: CollapseItemLayoutBinding) : RecyclerView.ViewHolder(row.root) {
        val itemLayout = row

    }

    class ChildViewHolder(row: ExpandableItemLayoutBinding) : RecyclerView.ViewHolder(row.root) {
        val itemLayout = row
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == 0) {
            val collapseItemLayoutBinding =
                CollapseItemLayoutBinding.inflate(LayoutInflater.from(parent.context))
            GroupViewHolder(collapseItemLayoutBinding)
        } else {
            val expandableItemLayoutBinding =
                ExpandableItemLayoutBinding.inflate(LayoutInflater.from(parent.context))
            ChildViewHolder(expandableItemLayoutBinding)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val modelData = list[position]
        if (modelData.parent_view == 0) {
            holder as GroupViewHolder
            holder.apply {
                itemLayout.carName.text = "${modelData.car_name} ${modelData.car_model}"
                itemLayout.carPrice.text =
                    "${mContext.resources.getString(R.string.price)} ${modelData.car_price}"
                itemLayout.carRating.rating = modelData.car_rating!!.toFloat()
                itemLayout.itemImage.setImageResource(modelData.car_image!!)
                itemLayout.expand.setOnClickListener {
                    expandOrCollapseParentItem(modelData, position)
                    if (itemLayout.view.visibility == View.VISIBLE) {
                        itemLayout.view.visibility = View.GONE
                    } else {
                        itemLayout.view.visibility = View.VISIBLE
                    }
                }
            }
        } else {
            holder as ChildViewHolder
            holder.apply {
                for (item in modelData.car_pros_cons!!.indices) {
                    if (!modelData.car_pros_cons!![item].car_pros.isNullOrEmpty()) {
                        itemLayout.tvPros.visibility = View.VISIBLE
                        itemLayout.recyclePool.visibility = View.VISIBLE
                        itemLayout.tvPros.text = modelData.car_pros_cons!![item].label
                        itemLayout.recyclePool.layoutManager = LinearLayoutManager(mContext)
                        itemLayout.recyclePool.setRecycledViewPool(viewPool)
                        itemLayout.recyclePool.adapter =
                            ProsConsRecyclerViewAdapter(modelData.car_pros_cons!![item].car_pros!!)
                    } else {
                        itemLayout.tvPros.visibility = View.GONE
                        itemLayout.recyclePool.visibility = View.GONE
                    }

                    if (modelData.car_pros_cons!![item].label.equals("cons:", true)) {
                        itemLayout.view.visibility = View.VISIBLE
                    } else {
                        itemLayout.view.visibility = View.GONE
                    }
                }
            }
        }
    }

    // check item if collapse or expand
    private fun expandOrCollapseParentItem(singleBoarding: CarData, position: Int) {
        if (singleBoarding.isExpanded!!) {
            collapseParentRow(position)
        } else {
            expandParentRow(position)
        }
    }

    // expand view handle
    private fun expandParentRow(position: Int) {
        val currentBoardingRow = list[position]
        val services = currentBoardingRow.car_pros_cons
        currentBoardingRow.isExpanded = true
        var nextPosition = position
        if (currentBoardingRow.parent_view == 0) {
            services?.forEach { service ->
                val parentModel = CarData(car_model = "")
                parentModel.parent_view = 1
                val subList: ArrayList<CarProsCons> = ArrayList()
                subList.add(service)
                parentModel.car_pros_cons = subList
                list.add(++nextPosition, parentModel)
            }
            notifyDataSetChanged()
        }
    }

    // Collapse view handle
    private fun collapseParentRow(position: Int) {
        val currentBoardingRow = list[position]
        val services = currentBoardingRow.car_pros_cons
        list[position].isExpanded = false
        if (list[position].parent_view == 0) {
            services?.forEach { _ ->
                list.removeAt(position + 1)
            }
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int = list.size

    override fun getItemViewType(position: Int): Int = list[position].parent_view!!

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    // list filter and notify
    fun filterCarList(filterList: MutableList<CarData>) {
        list = filterList
        notifyDataSetChanged()
    }
}