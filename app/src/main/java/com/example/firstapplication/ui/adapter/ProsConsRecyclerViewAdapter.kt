package com.example.firstapplication.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.firstapplication.databinding.PoolItemLayoutBinding

class ProsConsRecyclerViewAdapter(private val list: MutableList<String>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class GroupViewHolder(row: PoolItemLayoutBinding) : RecyclerView.ViewHolder(row.root) {
        val itemLayout = row
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val poolItemLayoutBinding =
            PoolItemLayoutBinding.inflate(LayoutInflater.from(parent.context))
        return GroupViewHolder(poolItemLayoutBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val modelData = list[position]
        holder as GroupViewHolder
        holder.apply {
            if (modelData.isNotEmpty()) {
                itemLayout.prosConsText.visibility = View.VISIBLE
                itemLayout.tvBullets.visibility = View.VISIBLE
                itemLayout.prosConsText.text = modelData
            } else {
                itemLayout.prosConsText.visibility = View.GONE
                itemLayout.tvBullets.visibility = View.GONE
            }
        }
    }

    override fun getItemCount(): Int = list.size

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }
}