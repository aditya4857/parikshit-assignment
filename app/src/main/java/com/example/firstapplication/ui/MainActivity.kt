package com.example.firstapplication.ui

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.firstapplication.databinding.ActivityMainBinding
import com.example.firstapplication.model.CarData
import com.example.firstapplication.model.FilterDataModel
import com.example.firstapplication.ui.adapter.RecyclerViewAdapter
import com.example.firstapplication.viewModel.MainActivityViewModel


class MainActivity : AppCompatActivity() {

    private lateinit var mainActivityViewModel: MainActivityViewModel
    private lateinit var binding: ActivityMainBinding
    private lateinit var recyclerViewAdapter: RecyclerViewAdapter
    private var carDataList = ArrayList<CarData>()
    private var filterDataModel = FilterDataModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        mainActivityViewModel =
            ViewModelProvider(this)[MainActivityViewModel::class.java] // attach view model
        mainActivityViewModel.getCarListData(this) // call json car data from json file or local DB

        binding.exRecycle.layoutManager = LinearLayoutManager(this) // set layout manager

        /* getting car list observer */
        mainActivityViewModel.getCarList().observe(this) {
            if (!it.isNullOrEmpty()) {
                carDataList.addAll(it)
                recyclerViewAdapter.notifyItemInserted(carDataList.size - 1)
            }
        }

        /* Text Watcher of make */
        binding.etMake.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                if (p0.toString().trim().isNotEmpty()) {
                    filterDataModel.car_make = p0.toString()
                    filterList(filterDataModel)
                } else {
                    filterDataModel.car_make = ""
                    filterList(filterDataModel)
                }
            }
        })

        /* Text Watcher of Model */
        binding.etModel.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                if (p0.toString().trim().isNotEmpty()) {
                    filterDataModel.car_model = p0.toString()
                    filterList(filterDataModel)
                } else {
                    filterDataModel.car_model = ""
                    filterList(filterDataModel)
                }
            }
        })

        setAdapter()  // call adapter
    }

    /*
    * @Method create a object of recycler view
    * and set the adapter of recycler
    * */
    private fun setAdapter() {
        recyclerViewAdapter = RecyclerViewAdapter(this@MainActivity, carDataList)
        binding.exRecycle.adapter = recyclerViewAdapter
    }

    /*
    * @Method filter the list of cars with name and model
    * @param FilterDataModel (car_name, car_model)
    * */
    fun filterList(filterDataModel: FilterDataModel) {
        val listfiltered = ArrayList<CarData>()

        // car make and model is not empty
        if (!filterDataModel.car_make.isNullOrEmpty() && !filterDataModel.car_model.isNullOrEmpty()) {
            // for loop for filter
            for (item in carDataList) {
                // match car make and model for filter
                if ((item.car_name.toString().trim().lowercase()
                        .contains(filterDataModel.car_make!!.trim())) && (item.car_model.trim()
                        .lowercase().contains(filterDataModel.car_model!!.trim()))
                ) {
                    listfiltered.add(item)
                }
            }
        }  // car make is not empty
        else if (!filterDataModel.car_make.isNullOrEmpty()) {
            // for loop for filter
            for (item in carDataList) {
                // match car make for filter
                if ("${item.car_name?.trim()} ${item.car_model.trim()}".lowercase().contains(filterDataModel.car_make!!.trim())) {
                    listfiltered.add(item)
                }
            }
        }  // car model is not empty
        else if (!filterDataModel.car_model.isNullOrEmpty()) {
            // for loop for filter
            for (item in carDataList) {
                // match car model for filter
                if (item.car_model.trim().lowercase()
                        .contains(filterDataModel.car_model!!.trim())
                ) {
                    listfiltered.add(item)
                }
            }
        } else {
            listfiltered.addAll(carDataList)
        }
        recyclerViewAdapter.filterCarList(listfiltered)
    }
}