package com.example.firstapplication.local_db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.firstapplication.model.CarData
import com.example.firstapplication.model.ProsConsConverter

@Database(entities = [CarData::class], version = 1, exportSchema = true)
@TypeConverters(ProsConsConverter::class)
abstract class MyRoomDataBase : RoomDatabase() {
    abstract fun carListDao(): CarListDao?

    companion object {
        private var INSTANCE: MyRoomDataBase? = null

        fun getAppDatabase(context: Context): MyRoomDataBase? {
            if (INSTANCE == null) {
                INSTANCE =
                    Room.databaseBuilder(context, MyRoomDataBase::class.java, "car-db").build()
            }
            return INSTANCE
        }
    }
}