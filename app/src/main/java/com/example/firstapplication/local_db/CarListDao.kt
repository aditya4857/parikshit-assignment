package com.example.firstapplication.local_db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.firstapplication.model.CarData

@Dao
interface CarListDao {

    @Insert(onConflict = OnConflictStrategy.ABORT)
    suspend fun insertAll(carData: List<CarData>)

    @Query("SELECT * FROM car_data")
    suspend fun getCarList(): MutableList<CarData>

}