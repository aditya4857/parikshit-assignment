package com.example.firstapplication.viewModel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.firstapplication.model.CarData
import com.example.firstapplication.repository.CarListRepository.Companion.getRepositoryInstance
import kotlinx.coroutines.launch

class MainActivityViewModel : ViewModel() {

    private val carListJson = MutableLiveData<MutableList<CarData>>()

    /*
    * load json from assets and local db
    * @params
    * @context
    * */
    fun getCarListData(context: Context) {
        viewModelScope.launch {
            carListJson.value = getRepositoryInstance()?.loadJSON(context)
        }
    }

    /*
    * @Method provided a livedata of mutablelist
    * @return livedata of observing
    * */
    fun getCarList(): LiveData<MutableList<CarData>> {
        return carListJson
    }
}